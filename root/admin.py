from django.contrib import admin
from .models import *

admin.site.register(DogModel)
admin.site.register(OwnerModel)
