from django.db import models


class OwnerModel(models.Model):
    name = models.CharField(max_length=120)
    address = models.TextField(null=True, blank=True)
    number = models.CharField(max_length=9, null=True, blank=True)

    def __str__(self):
        return self.name


class DogModel(models.Model):
    name = models.CharField(max_length=45)
    age = models.SmallIntegerField()
    breed = models.CharField(max_length=45)
    owner = models.ForeignKey(OwnerModel, on_delete=models.CASCADE,
                              null=True, blank=True)

    def __str__(self):
        return self.name
