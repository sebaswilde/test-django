from django.urls import path

from .views import (
    IndexView,
    FrontendView,
    BackendView,
    CreateDog,
    ListDog,
    Front2View,
    Back2View,
    get_git_users,
)


urlpatterns = [
    path('', IndexView.as_view(), name="index"),
    path('frontend/', FrontendView.as_view(), name="frontend"),
    path('backend/', BackendView.as_view(), name="backend"),
    path('backend/dog/add/', CreateDog.as_view(), name='add-dog'),
    path('backend/dog/list/', ListDog.as_view(), name='list-dog'),
    path('frontend/2/', Front2View.as_view(), name='front2'),
    path('backend/2', Back2View.as_view(), name='back2'),
    path('get_git_user/', get_git_users, name='get_git_user'),
]
