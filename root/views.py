from django.urls import reverse_lazy
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.generic import View, CreateView, ListView
from django.core import serializers
from .models import DogModel
from .forms import DogForm
import requests as res
import json


class IndexView(View):
    template_name = "index.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class FrontendView(View):
    template_name = "frontend.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class BackendView(View):
    template_name = "backend.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class CreateDog(CreateView):
    model = DogModel
    success_url = reverse_lazy('list-dog')
    template_name = 'add_dog.html'
    form_class = DogForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['selected'] = 'add-dog'
        return context


class ListDog(ListView):
    model = DogModel
    template_name = 'list_dog.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['selected'] = 'list-dog'
        return context


class Front2View(View):
    template_name = "frontend2.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class Back2View(View):
    template_name = 'backend2.html'

    def get(self, request):
        search = request.GET.get('search', None)
        context = dict()
        if search:
            url = 'https://api.github.com/search/users?q={0}&per_page=20'.\
                format(search)
            r = res.get(url)
            if r.status_code == 200:
                data = r.json()
                if data['total_count'] > 0:
                    objects = data['items']
                    for obj in objects:
                        extra = res.get(obj['url'])
                        obj['more'] = extra.json()
                    context['objects'] = objects
        return render(request, self.template_name, context)


def get_git_users(request):
    search = request.GET.get('search', None)
    data = dict()
    if search:
        url = 'https://api.github.com/search/users?q={0}&per_page=5'. \
            format(search)
        r = res.get(url)
        if r.status_code == 200:
            objects = r.json()
            if objects['total_count'] > 0:
                data = objects['items']
                print(type(data))
    return JsonResponse(data, safe=False)

