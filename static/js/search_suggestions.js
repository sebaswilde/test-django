(function () {
    var options = {

        url: function(search) {
            return "/get_git_user/";
        },

        getValue: function(element) {
            return element.login;
        },

        ajaxSettings: {
            dataType: "json",
            method: "GET",
            data: {
                // dataType: "json"
            }
        },

        preparePostData: function(data) {
            data.search = $("#search").val();
            return data;
        },

        requestDelay: 1000
    };

    $("#search").easyAutocomplete(options);
})(jQuery);